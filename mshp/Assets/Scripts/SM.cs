using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SM : MonoBehaviour
{
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "next_lvl")
          SceneManager.LoadScene("Scene1");
    }
    Rigidbody2D rb;
    public bool scene = true;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        
    }

    // Update is called once per frame
    void Update()
    {
        
        if (gameObject.transform.position.x > 9f)
        {
            SceneManager.LoadScene("Scene1");
        }
        else if (scene == false)
        {
            Debug.Log("Winner");
        }
    }
}
